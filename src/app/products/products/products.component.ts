import { Subscription } from 'rxjs/Rx';
import { GetJsonService } from '../get-json.service';
import { Response } from '@angular/http';
import { Component, OnInit } from '@angular/core';
import { OnDestroy } from '@angular/core/src/metadata/lifecycle_hooks';

@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.css']
})
export class ProductsComponent implements OnInit, OnDestroy {
  subcription: Subscription;
  private data;
  address: string[];
  addressIndex = 0;
  gstExclu: number;
  grandTotal: number;
  subTotal: any;


  constructor(private getJsonService: GetJsonService) { }

  ngOnInit() {
    this.subcription = this.getJsonService.getJson().subscribe(
      (response: Response) => {
        this.data = response.json();
        this.address = this.data.customer.addressList[this.addressIndex];

        this.grandTotal = (this.data['customer']['basket']['grandTotal']).substr(1);
        this.subTotal = (((this.data['customer']['basket']['subTotal']).substr(1)) / 11).toFixed(2);
        this.gstExclu = (this.grandTotal) - (this.subTotal);
      }
    );
  }

  onAddressClick(id: number) {
    this.address = this.data.customer.addressList[id];
    console.log(this.address);
  }

  ngOnDestroy() {
    this.subcription.unsubscribe();
  }
}
