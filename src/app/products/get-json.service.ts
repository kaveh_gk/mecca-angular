import { Injectable } from '@angular/core';
import { Http } from '@angular/http';

@Injectable()
export class GetJsonService {

  constructor(private http: Http) { }

  getJson() {
    return this.http.get('../../assets/basket.json');
  }
}
