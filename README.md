# MeccaAngular

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 1.3.2.

It is written completely on angular and no jQuery plugin or library like Bootstrap.js is used. Ng-bootstrap (Which is completely angular package)
has been used to support Tooltip feature.

## Development server

To run the project, please first run "npm install @angular/cli" and then run "npm install --save @ng-bootstrap/ng-bootstrap".

## In The Future
It is going to be deployed on trial server as soon as possible.


Thanks